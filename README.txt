
-- SUMMARY --

See the project page.

-- REQUIREMENTS --

Services - 6.x-0.15
soap server - 6.x-1.2-beta1


-- INSTALLATION --

* Install as usual.


-- CONFIGURATION --

* Configure user permissions in Administer >> User management >> Permissions >>
  services

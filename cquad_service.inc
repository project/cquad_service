<?php

/**
 * @author CIGNEX Dev team
 * @file
 *  General node functionalities to CQUAD services module.
 */

/**
 * Returns list of node data
 *
 * @param $content_type
 *   String. The node type .
 * @return
 *   Array. The array of nodes with nid, title, teaser.
 */
function cquad_service_data($content_type) {
  $result = db_query("SELECT nid from {node} where type LIKE '%s'", $content_type);
  $data = array();
  $nodedata = array();
  while ($nid = db_fetch_array($result)) {
    $node = node_load($nid);
    $nodedata['nid'] = $node->nid;
    $nodedata['title'] = $node->title;
    $nodedata['teaser'] = $node->teaser;
    $data[] = $nodedata;
  }
  if ($data) {
    return $data;
  }
  else {
    return services_error(t('No results.'));
  }
}

/**
 * Returns list of node data
 *
 * @return
 *   Array. The array of content type name type as their keys
 */
function cquad_service_content_type() {
  $query = db_query("SELECT nt.name, nt.type FROM {node_type} nt ");
  $data = array();
  while ($results = db_fetch_array($query)) {
    $result = array();
    $result['node_name'] = $results['name'];
    $result['node_type'] = $results['type'];
    array_push($data, $result);
  }
  if ($data) {
    return $data;
  }
  else {
    return services_error(t('No results.'));
  }
}
